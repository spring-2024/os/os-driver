#include <dils.h>
int BUFFER_SIZE;
sfs_superblock *super;

// gcc -I. -o dils dils.c functions.c driver.c bitmap.c && ./dils initrd -l

int main(int argc, char *argv[])
{
    void (*callback)(sfs_dirent);

    if (argc < 2)
    {
        printf("Not enough arguments\nUsage: [disk image] [-l if you want a long listing]\n");
        return 1;
    }

    if (argc == 2)
    {
        callback = ls;
    }
    else if (argc == 3 && strcmp(argv[2], "-l") == 0)
    {
        callback = lsLong;
    }

    else {
        printf("To many argument\nUsageUsage: [disk image] [-l if you want a long listing]\n");
        return 1;
    }

    BUFFER_SIZE = 128;
    /* declare a buffer that is the same size as a filesystem block */
    char raw_superblock[BUFFER_SIZE];

    /* Create a pointer to the buffer so that we can treat it as a
        superblock struct. The superblock struct is smaller than a block,
        so we have to do it this way so that the buffer that we read
        blocks into is big enough to hold a complete block. Otherwise the
        driver_read function will overwrite something that should not be
        overwritten. */
    super = (sfs_superblock *)raw_superblock;

    /* open the disk image and get it ready to read/write blocks */
    driver_attach_disk_image(argv[1], 128);

    /* Read blocks from the disk image */
    findSuperBlock();

    sfs_inode_t inodes[2];
    driver_read(inodes, super->inodes);

    sfs_inode_t root_inode = inodes[0];

    getFiles(root_inode, super, callback, NULL);
    /* close the disk image */
    driver_detach_disk_image();

  return 0;
}