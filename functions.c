#include <functions.h>
//gcc -I. -o dils dils.c functions.c driver.c bitmap.c && cp initrd initrd_org && gcc -I. -o testing testing.c functions.c driver.c bitmap.c && ./testing initrd && ./dils initrd

int SINGLE_INDIRECT = 5;
int DOUBLE_INDIRECT = 37;
int TRIPLE_INDIRECT = 5 + 32 + 32 * 32;

void ls(sfs_dirent dir)
{
  printf("%s\n", dir.name);
}

void lsLong(sfs_dirent dir) {
  sfs_inode_t inode; 
  getINode(&inode, dir.inode, super->inodes);
  char* filename = dir.name;
  /* 
  Permissions
  type
  owner
  group
  size
  time
  filename
   */

  printPermission(inode.perm, inode.type);
  printf(" %2d ", inode.refcount);
  printf("%4d ", inode.owner);
  printf("%4d ", inode.group);
  printf("%7ld ", inode.size);
  printTime(inode.mtime);
  printf("%s\n", filename);
}

void printTime(uint32_t time)
{
  time_t epoch_time = time; // assuming inode.mtime is the epoch timestamp
  struct tm *time_info;
  char buffer[80];

  time_info = localtime(&epoch_time);

  strftime(buffer, 80, "%b %d %H:%M %Y", time_info);
  printf("%s ", buffer);
}

void printPermission(uint16_t perm, uint8_t dir)
{
  char type;
  switch (dir)
  {
  case 1:
    type = 'd';
    break;
  case 2:
    type = 'c';
    break;
  case 3:
    type = 'b';
    break;
  case 4: 
    type = 'p';
    break;
  case 5:
    type = 's';
    break;
  case 6:
    type = 'l';
    break;
  default:
    type = '-';
    break;
  }
  printf("%c", type);
  printf( (perm & 0400) ? "r" : "-");
  printf( (perm & 0200) ? "w" : "-");
  printf( (perm & 0100) ? "x" : "-");
  printf( (perm & 0040) ? "r" : "-");
  printf( (perm & 0020) ? "w" : "-");
  printf( (perm & 0010) ? "x" : "-");
  printf( (perm & 0004) ? "r" : "-");
  printf( (perm & 0002) ? "w" : "-");
  printf( (perm & 0001) ? "x" : "-");
}

void getINode(sfs_inode_t *n, uint32_t blockNum, int baseInode)
{
  sfs_inode_t inodes[2];
  int isOdd = blockNum % 2;
  int index = blockNum / 2;

  driver_read(inodes, baseInode + index);

  *n = inodes[isOdd];
}

void getFileBlock(sfs_inode_t *n, uint32_t block_num, char *data)
{ // Max block size is 128

  uint32_t ptrs[32];
  if (block_num < 5)
  {
    driver_read(data, n->direct[block_num]);
  }
  else if (block_num < 37) // indirect
  {
    driver_read(ptrs, n->indirect);
    driver_read(data, ptrs[block_num - 5]);
  }
  else if (block_num < 5 + 32 + 32 * 32) // Double indirect
  { // two blocks deep
    driver_read(ptrs, n->dindirect); // first block that indirect containts
    uint32_t tmp = (block_num - 5 - 32) / 32;
    driver_read(ptrs, ptrs[tmp]); // second block ptrs
    tmp = (block_num - 5 - 32) % 32;
    driver_read(data, ptrs[tmp]); // Read the data block
  }
  else {
    // triple indirect
    //5 + 32 direct + 32^2 double indirect + 32^3 Triple indirect
    // Each block is 128.
    // Our max file size is 4mb
    driver_read(ptrs, n->tindirect);
    uint32_t tmp = (block_num - 5 - 32 - 32 * 32) / (32 * 32);
    driver_read(ptrs, ptrs[tmp]);
    tmp = (block_num - 5 - 32 - 32 * 32) % (32 * 32) / 32;
    driver_read(ptrs, ptrs[tmp]);
    tmp = (block_num - 5 - 32 - 32 * 32) % (32 * 32) % 32;
    driver_read(data, ptrs[tmp]);
  }
}

void writeFileBlock(sfs_inode_t *n, uint32_t dataBlockNum, uint32_t dataBlockLocation)
{ // Max block size is 128
  uint32_t ptrs[32], ptrs2[32], ptrs3[32];
  if (dataBlockNum < 5)
  {
    n->direct[dataBlockNum] = dataBlockLocation;
  }
  else if (dataBlockNum < 37) // indirect
  {
    driver_read(ptrs, n->indirect);
    ptrs[dataBlockNum - 5] = dataBlockLocation;
    driver_write(ptrs, n->indirect);
  }
  else if (dataBlockNum < 5 + 32 + 32 * 32) // Double indirect
  { // two blocks deep
    if (n->dindirect == -1)
    {
      makeDIndirect(n);
    }

    driver_read(ptrs, n->dindirect); // first block that indirect containts
    uint32_t tmp = (dataBlockNum - 5 - 32) / 32, tmp2;
    driver_read(ptrs2, ptrs[tmp]); // second block ptrs
    tmp2 = (dataBlockNum - 5 - 32) % 32;
    ptrs2[tmp2] = dataBlockLocation; // Read the data block
    driver_write(ptrs2, ptrs[tmp]);
  }
  else {
    // triple indirect
    //5 + 32 direct + 32^2 double indirect + 32^3 Triple indirect
    // Each block is 128.
    // Our max file size is 4mb
    if (n->tindirect == -1)
    {
      makeTIndirect(n);
    }
    driver_read(ptrs, n->tindirect);
    uint32_t tmp = (dataBlockNum - 5 - 32 - 32 * 32) / (32 * 32), tmp2, tmp3;
    driver_read(ptrs2, ptrs[tmp]);
    tmp2 = (dataBlockNum - 5 - 32 - 32 * 32) % (32 * 32) / 32;
    driver_read(ptrs3, ptrs2[tmp2]);
    tmp3 = (dataBlockNum - 5 - 32 - 32 * 32) % (32 * 32) % 32;
    ptrs3[tmp3] = dataBlockLocation;
    driver_write(ptrs3, ptrs2[tmp2]);
  }
}

void checkIfFileMatchesFileName(sfs_dirent dir, char* filename, void (*callback)(sfs_dirent), int *flag)
{
  if (strcmp(dir.name, filename) == 0)
  {
    callback(dir);
    *flag |= 1;
  }
}

void getFileContents(sfs_dirent dir)
{
  FILE *file = fopen(dir.name, "w");
  if (file == NULL)
  {
    printf("Error opening file!\n");
    exit(1);
  }

  sfs_inode_t inode;
  getINode(&inode, dir.inode, super->inodes);
  char data[super->block_size];
  for (int i = 0; i < inode.size; i += super->block_size)
  {
    int size_to_read = super->block_size;
    if (i + super->block_size > inode.size)
    {
        size_to_read = inode.size % super->block_size; // Prevent use from overstepping our bounds
    }

    getFileBlock(&inode, i / super->block_size, data);
    fwrite(data, sizeof(char), size_to_read, file);
  }
}

void getFiles(sfs_inode_t root_inode, sfs_superblock *super, void (*callback)(sfs_dirent), char* filename)
{
  int dirsInBlock = super->block_size / sizeof(sfs_dirent);
  sfs_dirent dirs[dirsInBlock];
  sfs_inode_t fileInode;
  int found = 0;

  int inodesInRootDir = root_inode.size / sizeof(sfs_dirent);

  for (int i = 0; i * dirsInBlock < inodesInRootDir; i++)
  {
    getFileBlock(&root_inode, i, (char *)&dirs);

    for (int j = 0; j < dirsInBlock && i * dirsInBlock + j < inodesInRootDir; j++)
    {
      getINode(&fileInode, dirs[j].inode, super->inodes);
      if (filename != NULL)
      {
        checkIfFileMatchesFileName(dirs[j], filename, callback, &found);
      }
      else {
        callback(dirs[j]);
        found = 1;
      }
      
    }
  }

  if (!found)
  {
    printf("File not found\n");
  }
}

void findSuperBlock()
{
  int superblock_found = -1;
  for (int i = 0; i < BUFFER_SIZE; i++)
  {
    driver_read(super, i);

    if (super->fsmagic == VMLARIX_SFS_MAGIC &&
      ! strcmp(super->fstypestr,VMLARIX_SFS_TYPESTR))
      {
        superblock_found = i;
        break;
      }
  }

  if (superblock_found == -1)
  {
    printf("superblock not found\n");
    exit(1);
  }
}


int write_helloworld(char *filename, sfs_inode_t *root_inode) {
  // 1. Update Inode Information
  int inode_num = first_cleared(&super->fi_bitmap, super->num_inodes); // Find free inode
  if (inode_num == -1) return -1; // No free inodes
  set_bit(&super->fi_bitmap, inode_num); // Mark inode as allocated


  sfs_inode_t inode;
  inode.type = FT_NORMAL; // Regular file
  inode.perm = 0644; // Permissions: owner read/write, others read-only 
  inode.size = 13; // 12 characters + null terminator

  int blockNum = first_cleared(&super->fb_bitmap, super->num_blocks); // Find free block
  if (blockNum == -1) return -1; // No free blocks
  set_bit(&super->fb_bitmap, blockNum); // Mark block as allocated
  inode.direct[0] = blockNum; // Store block number in inode

  // 2. Write Data Block
  char data[super->block_size];
  strcpy(data, "Hello, world!"); // Write data to block
  driver_write(data, blockNum); // Write block to disk

  // 3. Update Directory Entry (Assuming root directory for simplicity)
  sfs_dirent entry;
  strcpy(entry.name, filename);
  entry.inode = inode_num;

  sfs_dirent dirs[4];
  getFileBlock(root_inode, 1, (char*) dirs);
  dirs[2] = entry;
  root_inode->size += sizeof(sfs_dirent);
  driver_write(dirs, root_inode->direct[1]);
  

  // 4. Update Superblock and Inode Table
  super->inodes_free--;
  super->blocks_free--;
  driver_write(super, super->superblock); // Write updated superblock
  //return 0;
  
  

  // Write the inode to the inode table
  int inode_block = inode_num / 2; // Calculate block containing the inode
  int inode_index = inode_num % 2; // Index within the block (0 or 1)
  sfs_inode_t inode_block_data[2];
  driver_read(inode_block_data, super->inodes + inode_block); // Read existing inodes in the block
  inode_block_data[inode_index] = inode; // Place the new inode into the block
  driver_write(inode_block_data, super->inodes + inode_block); // Write back the block
  
  driver_read(inode_block_data, super->inodes); // Read existing inodes in the block
  inode_block_data[0] = *root_inode; // Place the root_dir inode into the block
  driver_write(root_inode, super->inodes); // Write back the block
  
  return 0; // Success
}

int writeInode(sfs_inode_t *newInode, sfs_inode_t *root_inode, uint32_t blockNum)
{
  int inode_num = blockNum;

  // 4. Update Superblock and Inode Table
  super->inodes_free--;
  driver_write(super, super->superblock); // Write updated superblock
  
  // Write the inode to the inode table
  int inode_block = inode_num / 2; // Calculate block containing the inode
  int inode_index = inode_num % 2; // Index within the block (0 or 1)
  sfs_inode_t inode_block_data[2];
  driver_read(inode_block_data, super->inodes + inode_block); // Read existing inodes in the block
  inode_block_data[inode_index] = *newInode; // Place the new inode into the block
  driver_write(inode_block_data, super->inodes + inode_block); // Write back the block
  
  driver_read(inode_block_data, super->inodes); // Read existing inodes in the block
  inode_block_data[0] = *root_inode; // Place the root_dir inode into the block
  driver_write(inode_block_data, super->inodes); // Write back the block
  return inode_num;
}

int writeDataToImage(char* filename, int size, sfs_inode_t *root_node, sfs_inode_t* baseInode)
{
  int blockNum = claimBlock();

  // 2. Write Data Block
  char data[super->block_size];
  strcpy(data, filename); // Write data to block
  driver_write(data, blockNum); // Write block to disk

  return blockNum;
}

int32_t claimBlock()
{
  uint32_t block = super->fb_bitmap;
  uint32_t bitmaps[32];
  
  int32_t blockNum = -1;

  while (block < super->fb_bitmapblocks + super->fb_bitmap && blockNum == -1)
  {
    driver_read(bitmaps, block); // Read the first four bitmaps
    
    for (int i = 0; i < 32; i++)
    {
      
      blockNum = first_cleared(&bitmaps[i], BITMAP_BITS); // Find free block
      if (blockNum != -1)
      {
        set_bit(&bitmaps[i], blockNum); // Mark block as allocated
        blockNum += (block - super->fb_bitmap) * BITMAP_BITS * 32 + i * BITMAP_BITS;

        driver_write(bitmaps, block); // Write back the block
        return blockNum;
      }
    }

    block++;
  }
    return blockNum;
}

int claimNode()
{
  uint32_t block = super->fi_bitmap;
  uint32_t bitmaps[32];
  
  int32_t blockNum = -1;

  while (block < super->fi_bitmapblocks + super->fi_bitmap && blockNum == -1)
  {
    driver_read(bitmaps, block); // Read the first four bitmaps
    
    for (int i = 0; i < 32; i++)
    {
      
      blockNum = first_cleared(&bitmaps[i], BITMAP_BITS); // Find free block
      if (blockNum != -1)
      {
        set_bit(&bitmaps[i], blockNum); // Mark block as allocated
        blockNum += (block - super->fi_bitmap) * BITMAP_BITS * 32 + i * BITMAP_BITS;

        driver_write(bitmaps, block); // Write back the block'
        return blockNum;
      }
    }

    block++;
  }
  
  return blockNum;
}

void makePtrs(sfs_inode_t *inode)
{
  int blockNum = claimBlock();
  uint32_t ptrs[32];
  for (int i = 0; i < 32; i++)
  {
    ptrs[i] = 0;
  }
  inode->indirect = blockNum;
  driver_write(ptrs, inode->indirect);

  inode->dindirect = -1;
  inode->tindirect = -1;
}

void makeDIndirect(sfs_inode_t *inode)
{
  int blockNum = claimBlock();
  uint32_t ptrs[32], p2[32];
  for (int i = 0; i < 32; i++)
  {
    ptrs[i] = claimBlock();
    for (int j = 0; j < 32; j++)
    {
      p2[j] = 0;
    }
    driver_write(p2, ptrs[i]);
  }
  inode->dindirect = blockNum;
  driver_write(ptrs, inode->dindirect);
}

void makeTIndirect(sfs_inode_t *inode)
{
  int blockNum = claimBlock();
  uint32_t ptrs[32], p2[32], p3[32];
  for (int i = 0; i < 32; i++)
  {
    ptrs[i] = claimBlock();
    for (int j = 0; j < 32; j++)
    {
      p2[j] = claimBlock();
      for (int k = 0; k < 32; k++)
      {
        p3[k] = 0;
      }
      driver_write(p3, p2[j]);
    }
    driver_write(p2, ptrs[i]);
  }
  inode->tindirect = blockNum;
  driver_write(ptrs, inode->tindirect);
}

void writeFileToImage(char* filename, sfs_inode_t *root_inode)
{
  FILE* file = fopen(filename, "r");
  if (file == NULL) {
      perror("fopen");
      return;
  }

    char buffer[128];
    size_t bytes_read;

    sfs_inode_t newInode;
    makePtrs(&newInode);
    newInode.type = FT_NORMAL; // Regular file
    newInode.perm = 0644; // Permissions: owner read/write, others read-only
    int i = 0, blockLocation;
  
    do {
        bytes_read = fread(buffer, 1, sizeof(buffer), file);

        blockLocation = writeDataToImage(buffer, bytes_read, root_inode, &newInode);
        writeFileBlock(&newInode, i, blockLocation);
        super->blocks_free--;

        newInode.size += bytes_read;
        i++;

    } while (bytes_read == sizeof(buffer));
    fclose(file);

    int inode_num = claimNode();
    
    sfs_dirent entry;
    strcpy(entry.name, filename);
    entry.inode = inode_num;
    updateRotDir(root_inode, &entry);

    writeInode(&newInode, root_inode, inode_num);
    return;

    
}

void updateRotDir(sfs_inode_t *root_inode, sfs_dirent *dir)
{
  int dirsInBlock = super->block_size / sizeof(sfs_dirent);
  sfs_dirent dirs[dirsInBlock];
  int inodesInRootDir = root_inode->size / sizeof(sfs_dirent);
  int calced = inodesInRootDir / 4; // dont need to add one as its zero indexed
  if (calced % 4 == 0)
  {
    writeFileBlock(root_inode, calced, claimBlock());
  }
  int claimed;
  getFileBlock(root_inode, calced, (char*) dirs);
  
  for (int i = 0; i < dirsInBlock; i++)
  {
    if (dirs[i].name[0] == '\0' || dirs[i].name[0] == '\xa7')
    {
      dirs[i] = *dir;
      claimed = getInodeRedirectBlock(root_inode, calced);
      driver_write(dirs, claimed);
      break;
    }
  }

  root_inode->size += sizeof(sfs_dirent);

  
} 

int getInodeRedirectBlock(sfs_inode_t *n,int block_num)
{
  uint32_t ptrs[32];
  if (block_num < 5)
  {
    return n->direct[block_num];
  }
  else if (block_num < 37) // indirect
  {
    driver_read(ptrs, n->indirect);
    return ptrs[block_num - 5];
  }
  else if (block_num < 5 + 32 + 32 * 32) // Double indirect
  { // two blocks deep
    driver_read(ptrs, n->dindirect); // first block that indirect containts
    uint32_t tmp = (block_num - 5 - 32) / 32;
    driver_read(ptrs, ptrs[tmp]); // second block ptrs
    tmp = (block_num - 5 - 32) % 32;
    return ptrs[tmp]; // Read the data block
  }
  else {
    // triple indirect
    //5 + 32 direct + 32^2 double indirect + 32^3 Triple indirect
    // Each block is 128.
    // Our max file size is 4mb
    driver_read(ptrs, n->tindirect);
    uint32_t tmp = (block_num - 5 - 32 - 32 * 32) / (32 * 32);
    driver_read(ptrs, ptrs[tmp]);
    tmp = (block_num - 5 - 32 - 32 * 32) % (32 * 32) / 32;
    driver_read(ptrs, ptrs[tmp]);
    tmp = (block_num - 5 - 32 - 32 * 32) % (32 * 32) % 32;
    return ptrs[tmp];
  }
}