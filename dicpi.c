#include <functions.h>

int BUFFER_SIZE;
sfs_superblock *super;

//  gcc -I. -o dicpi dicpi.c functions.c driver.c

int main(int argc, char *argv[])
{

    BUFFER_SIZE = 128;
    /* declare a buffer that is the same size as a filesystem block */
    char raw_superblock[BUFFER_SIZE];

    /* Create a pointer to the buffer so that we can treat it as a
        superblock struct. The superblock struct is smaller than a block,
        so we have to do it this way so that the buffer that we read
        blocks into is big enough to hold a complete block. Otherwise the
        driver_read function will overwrite something that should not be
        overwritten. */
    super = (sfs_superblock *)raw_superblock;

    /* open the disk image and get it ready to read/write blocks */
    driver_attach_disk_image(argv[1], 128);

    /* Read blocks from the disk image */
    findSuperBlock();

    sfs_inode_t inodes[2];
    driver_read(inodes, super->inodes);

    sfs_inode_t root_inode = inodes[0];

    writeFileToImage(argv[2], &root_inode);

    /* close the disk image */
    driver_detach_disk_image();

  return 0;
}