# SFS OS Project
## Author: Oliver Schwab
## Class: CSC-458

# How to compile

## dils
> gcc -I. -Wall -o dils dils.c functions.c driver.c bitmap.c

This compiles dils to an executable.
It accepts one argument and one optional argument:
- The disk image to read from
- The optional long listing tag

Some examples are
> ./dils initrd -l

This would long list all files in the initrd image.  
or 

> ./dils rd2.img

This would list all files in the rd2.img image. Only a single space seperates each file.

## dicpo

> gcc -I. -Wall -o dicpo dicpo.c functions.c driver.c bitmap.c

This compiles dicpo to an executable.
It accepts two arguments: 
- the image file to read from
- the file to search for and copy out

Some examples are 
> ./dicpo rd2.img sfs_read.c

> ./dicpo rd3.img rd2.img

## dicpi
> gcc -I. -Wall -o dicpi dicpi.c functions.c driver.c bitmap.c

This compiles dicpo to an executable.
It accepts two arguments: 
- the image file to read from
- the file copy into the image file

Some examples are 
> ./dicpi initrd functions.c

> ./dicpi initrd README.md

## Notes about dcipi
This kinda works. I can copy any number of files in to initrd, but it breaks on the other images.
I got really far but ran out of time. I did most of the things required for dicpi.
It updates the bitmaps, the super block, and writes all the data so it can be copied back out.
It just has some small things I cant fix. And for some reason wont work on the other images.

On rd2 and rd3 it breaks resulting in 
```sfs_unlink.c
sfs_write.c
Q
Q
Master Boot Record
functions.c
functions.c
functions.c
�
�
�
�
Master Boot Record
functions.c
functions.c
functions.c
Master Boot Record
functions.c
functions.c
```

Where the inserted file breaks on the dir barrier. I cant fix it. But it does write the files and they are copiable.
It works fine in initrd. Please test there :)



# Other thing of note
I have all the functions besides main in functions.c. 
This is because a lot of functions are used in both dicpo and dils. 
So I just decided to keep all functions in the same file. 
As thats how I developed the program. 
I only seperated functionality at the end.

For some reason no matter which way I print printf("%7ld ", inode.size); 
It throughs an error about it being the wrong format.
If I do lld it breaks on ubuntu, if i do ld it breaks on macos. 
So I chose to use ld as I know you are compiling with ubuntu.
