#include <driver.h>
#include <sfs_superblock.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sfs_dir.h>
#include <sfs_inode.h>
#include <time.h>
#include <stdlib.h>
#include <bitmap.h>

extern sfs_superblock *super;
extern int BUFFER_SIZE;

void ls(sfs_dirent dir);
void lsLong(sfs_dirent dir);
void printTime(uint32_t time);
void printPermission(uint16_t perm, uint8_t dir);
void getINode(sfs_inode_t *n, uint32_t blockNum, int baseInode);
void getFileBlock(sfs_inode_t *n, uint32_t block_num, char *data);
void checkIfFileMatchesFileName(sfs_dirent dir, char* filename, void (*callback)(sfs_dirent), int *flag);
void getFileContents(sfs_dirent dir);
void getFiles(sfs_inode_t root_inode, sfs_superblock *super, void (*callback)(sfs_dirent), char* filename);
void findSuperBlock();
int updateFirstInodeDataBlock(sfs_inode_t *inode, uint32_t newInodeLocation);
int write_helloworld(char *filename, sfs_inode_t *root_inode);
void writeFileBlock(sfs_inode_t *n, uint32_t dataBlockNum, uint32_t dataBlockLocation);
int writeDataToImage(char* filename, int size, sfs_inode_t *root_node, sfs_inode_t* baseInode);
int writeInode(sfs_inode_t *newInode, sfs_inode_t *root_inode, uint32_t blockNum);
void makePtrs(sfs_inode_t *inode);
void makeTIndirect(sfs_inode_t *inode);
void makeDIndirect(sfs_inode_t *inode);
void makePtrs(sfs_inode_t *inode);
void writeFileToImage(char* filename, sfs_inode_t *root_inode);
int claimNode();
int32_t claimBlock();
void updateRotDir(sfs_inode_t *root_inode, sfs_dirent *dir);
int getInodeRedirectBlock(sfs_inode_t *n,int block_num);
